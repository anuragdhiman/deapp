var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var CustomError = require('./server/Error');
require('./server/services/GlobalData');
var customSocket = require('./server/common_modules/utils/Socket');

// test data


/*var data = {person:"user",id:1,location:"delhi"};
global.PCSS_TREE.addDataIntoCluster("54321",data); //1 user in cluster 54321
global.PCSS_TREE.addDataIntoCluster("54322",data); //1 user in cluster 54322
global.PCSS_TREE.addDataIntoCluster("54323",data); //1 user in cluster 54323
global.PCSS_TREE.addDataIntoCluster("61323",data); //1 user in cluster 61323
global.PCSS_TREE.numberOfClustersAvailable(); //4
global.PCSS_TREE.getAllClustersData();		   // 4 cluster's data 54321,54322,54323 and 61323 
global.PCSS_TREE.getPincodeData("54321");

*/
// start the server
var app = express();
var port = process.env.PORT || 3000;
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies

//app.listen(port);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


/*
* Load from here all configs
*/
require('./server/routes/Config')(app);


/*var Message = require('./middleware/sms/sms_gateway');
var sms = new Message();
sms.sendSMS(76589642300, "SEND", function(response, body){
   console.log(response.statusCode);
   console.log(body);
});*/


/* catch 404 and forward to error handler */
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


/*
* error handlers
* development error handler
* will print stacktrace
*/
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        console.log(222244411);
        var error = new CustomError(res, err);
        error.print();
    });
}


/*
* production error handler
* no stacktraces leaked to user
*/
app.use(function(err, req, res, next) {
    console.log(11111111);
    var error = new CustomError(res, err);
    error.print();
});





new customSocket(app, port);
// listen (start app with node server.js) ======================================

app.get('/customers', function (req, res) {
    connection.query('select * from discounts', function (error, results, fields) {
        if (error) throw error;
        res.end(JSON(results));
    });
});




module.exports = app;
