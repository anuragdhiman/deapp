var app = angular.module('MainModule', [])
    // inject the service factory into our controller


app.controller('mainController', ['$scope', '$http', function($scope, $http) {
	var _this = $scope;
	
	_this.selection = "CompanyList";
	
	_this.onCompanyDetails = function (company) {
        console.log(company);
		_this.selection = "Company";
		_this.company = company;
	};
	_this.onBackToListStock = function () {
		_this.selection = "CompanyList";
	}
}]);

app.factory('stock', ['$http', 'CONFIG', function ($http, CONFIG) {
	var stock = {};
	
	stock.getData = function (index, size) {
		return $http({
			url: CONFIG.baseURL + CONFIG.stock,
			method: "GET",
			params: {start: index, size: size}
		});
	}
	
	stock.getCompanyDumpData = function (compyCode) {
		return $http({
			url: CONFIG.baseURL + CONFIG.stock + '/' + CONFIG.company + '/' + compyCode,
			method: "GET"
		});
	};

    stock.getCompanyRealData = function (compyCode) {
        return $http({
            url: CONFIG.baseURL + CONFIG.REAL_TIME,
            method: "GET",
            params: {code: compyCode, cfunction: 'TIME_SERIES_DAILY'}
        });
    };
	
	return stock;
}]);

app.directive('company', ['stock', function(stock) {
	var directive         = {};
	directive.restrict    = 'E';
	directive.templateUrl = '/js/partials/company.html';
	
	directive.link  = link;
	
	directive.scope = {
	    company: '=',
		backToListStock: '&'
	};
	
	function link(scope, element, attrs) {
	    'use strict';
		
		scope.query = {
			order: 's',
			limit: 10,
			page: 1
		};
		
		scope.companyDetails = {};
		scope.isDataLoading = true;
        scope.selectedDataTime = 'dump';
		scope.jumpToIndex = '';

		
		scope.goBackToListStock = function () {
            scope.backToListStock();
		}

        scope.loadDataSet = function() {
            console.log(scope.selectedDataTime);
            scope.isDataLoading = true;
            if(scope.selectedDataTime === 'real') {
                getCompanyRealData();
			} else {
                getCompanyDumpData();
			}
        }
        scope.jumpToSpecificindex = function(index) {
			if(Math.ceil(scope.companyDetails.count / scope.query.limit) -1 >= index) {
                scope.query.page = index;
			}
		}
		scope.getTotalPage = function(count, limit){
			return Math.ceil(count / limit) -1;
		}
		function getCompanyDumpData() {
            stock.getCompanyDumpData(scope.company.symbol)
                .then(function (result) {
                    scope.isDataLoading = false;
                    if (!!result && !!result.data) {
                        scope.companyDetails.data = result.data;
                        scope.companyDetails.count = result.data.length;

                        convertData(angular.copy(result.data));

                    } else {
                        alert('Could not load data. Check developer console to know error');
                        console.error(result);
                    }
                }, function (err, msg) {
                    scope.isDataLoading = false;
                    alert('Could not load data. Check developer console to know error');
                    console.error(msg, err);
                });
        }

        function getCompanyRealData() {
            stock.getCompanyRealData(scope.company.symbol)
                .then(function (result) {
                    scope.isDataLoading = false;
                    if (!!result && !!result.data) {

                        var tempData = angular.copy(result.data["Time Series (Daily)"]);

                        var data = [];

                        angular.forEach(tempData, function (rval, rkey) {
                        	var tmp = {
                        		date: rkey
							};

                            angular.forEach(rval, function (val, key) {
								if(key.indexOf('open') != -1) {
                                    tmp.open = val;
								} else if(key.indexOf('close') != -1) {
                                    tmp.close = val;
                                } else if(key.indexOf('high') != -1) {
                                    tmp.high = val;
                                } else if(key.indexOf('low') != -1) {
                                    tmp.low = val;
                                } else if(key.indexOf('volume') != -1) {
                                    tmp.volume = val;
                                }
                            });

                            data.push(tmp);
                        });

                        console.error(data);

                        scope.companyDetails.data = data;
                        scope.companyDetails.count = data.length;

                        convertData(angular.copy(data));

                    } else {
                        alert('Could not load data. Check developer console to know error');
                        console.error(result);
                    }
                }, function (err, msg) {
                    scope.isDataLoading = false;
                    alert('Could not load data. Check developer console to know error');
                    console.error(msg, err);
                });
		}


		function convertData(result) {
			var data = [];
			angular.forEach(result, function (obj) {
                data.push([new Date(obj.date).getTime(), parseFloat(obj.high)]);
            });
            drawChart(data);
		}

		function drawChart(data) {
                Highcharts.chart('hightChartContainer', {
                    chart: {
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Stock rate for '+scope.company.symbol+' over time(' +  scope.selectedDataTime.toUpperCase() + ')'
                    },
                    subtitle: {
                        text: document.ontouchstart === undefined ?
                            'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
                    },
                    xAxis: {
                        type: 'datetime'
                    },
                    yAxis: {
                        title: {
                            text: 'Exchange rate'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        area: {
                            fillColor: {
                                linearGradient: {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0,
                                    y2: 1
                                },
                                stops: [
                                    [0, Highcharts.getOptions().colors[0]],
                                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                                ]
                            },
                            marker: {
                                radius: 2
                            },
                            lineWidth: 1,
                            states: {
                                hover: {
                                    lineWidth: 1
                                }
                            },
                            threshold: null
                        }
                    },

                    series: [{
                        type: 'area',
                        name: 'Rate(High)',
                        data: data
                    }]
                });
		}

        getCompanyDumpData();
	}
	
	return directive;
}]);

app.filter('toDate', function() {
    return function(string) {
        return new Date(string).getTime();
    }
});

app.directive('customTable', ['stock', '$timeout', function(stock, $timeout) {
	var directive = {};
	directive.restrict = 'E';
	directive.templateUrl = '/js/partials/table.html';
	
	directive.link = link;
	directive.scope = {
		onShowCompany: '&'
    }
	function link(scope, element, attrs) {
		scope.desserts = {};
        scope.jumpToIndex = '';

        scope.jumpToSpecificindex = function(index) {
            if(Math.ceil(scope.desserts.count / scope.query.limit) -1 >= index) {
                scope.query.page = index;
                scope.getStockData();
            }
        }
        scope.getTotalPage = function(count, limit){
            return Math.ceil(count / limit) -1;
        }

		scope.getStockData = function() {
		    'use strict';
			scope.isDataLoading = true;
			
			stock.getData(scope.query.page, scope.query.limit)
                .then(function(result){
                    if(!!result && !!result.data) {
						scope.desserts.data = result.data.data;
						scope.desserts.count = result.data.count;
					} else {
                        alert('Could not load data. Check developer console to know error');
                        console.error(result);
                    }
					scope.isDataLoading = false;
                }, function(err, msg){
					alert('Could not load data. Check developer console to know error');
					console.error(msg, err);
					scope.isDataLoading = false;
                });
        }
        scope.onRowClick = function(company) {
		    'use strict';
		    scope.onShowCompany({company: company});
        }
		
		scope.query = {
			order: 's',
			limit: 10,
			page: 1
		};
		
		scope.isDataLoading = false;
		scope.getStockData();
	}

	return directive;
}]);