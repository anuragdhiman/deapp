var app = angular.module('mainApp', ['ui.router','ngMaterial', 'ngAnimate', 'ngAria', 'ngMdIcons', 'ngMessages', 'md.data.table', 'MainModule']);

app.config(['$httpProvider', function($httpProvider) {

    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    
    
}]);

app.constant("CONFIG" , {
    baseURL: './',
    stock: 'stock',
    company: 'company',
    REAL_TIME: 'realtime/stock/company'
});
