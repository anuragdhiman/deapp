// set up ======================================================================
var express = require('express');
var app = express(); 						// create our app w/ express
var port = process.env.PORT || 8000; 				// set the port
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var mysql      = require('mysql');

// configuration ===============================================================
app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
app.use('/bower_components', express.static('bower_components'));
app.use(bodyParser.urlencoded({'extended': 'true'})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request

/***********
 * Host static websit
 */

/*
 * mysql
 */
//
// var connection = mysql.createConnection({
//     host     : 'localhost',
//     user     : 'root',
//     password : '  '
// });
//
//
// connection.connect(function(err){
//     if(!err) {
//         console.log("Database is connected ... nn");
//
//         connection.query('SHOW DATABases', function(err, rows, fields) {
//             connection.end();
//             if (!err)
//                 console.log('The solution is: ', rows);
//             else
//                 console.log('Error while performing Query.');
//         });
//
//
//     } else {
//         console.log("Error connecting database ... nn", err);
//     }
// });


// routes ======================================================================
require('./app/Routes.js')(app);





app.listen(port);

console.warn('Server is running at port: ', port)
