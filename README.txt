
// I was not able to find location-id which instagram needed to get media information.
// I read their API but cound not find any information how to get location-id
// So i tried to get it following below stratgy
1 - I can find latitude and longitude from google map
2 - then called below url passing latitude and longitude
	 https://api.instagram.com/v1/locations/search?lat=48.858844&lng=2.294351&access_token=ACCESS-TOKEN 
	 which will give me id ( i have doubt whether this id is location-id or not )
3 - For each id i'm calling below url which can give me list of photos in that particular area using id found in step 2
	https://api.instagram.com/v1/locations/{location-id}/media/recent?access_token=ACCESS-TOKEN	 



installation process
------------------------------------------

required software
1. - npm
2. - nodejs
3. - bower
4 - Mysql


Run following command
npm install
bower install 


You need to config Database setting in app/config.js file

and then to start application run below command from terminal
nodejs server.js

and on browser type
http://localhost:8000

Info - Make sure that port 8000 is not used by any other applciation except this application


