var SERVER = SERVER || {};
var mysql      = require('mysql');
var CONFIG = require('./config');

SERVER.MySql = function(){
}
SERVER.MySql.prototype.getConnection = function(db){
    var connection = mysql.createConnection({
        host     : CONFIG.host,
        user     : CONFIG.user,
        password : CONFIG.password,
        database: db
    });

    return connection;

}

module.exports = SERVER.MySql;