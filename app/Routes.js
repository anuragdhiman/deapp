var ResponseHandler = require('./ResponseHandler');
var MySql = require('./mysql');
const https = require('https');
var CONFIG = require('./config');

var api_request = require('request');

module.exports = function (app) {

    // add a record

    app.use(function(req, res, next) {
        // do logging
        console.log('Something is happening.');
        next(); // make sure we go to the next routes and don't stop here
    });


    app.get('/stock', function (request, response) {
        console.log('Get', request.body, request.params);
        var inputData = request.body;
        var start = request.param('start');
        var size = request.param('size');

        var _mysql = new MySql();
        var connection =  _mysql.getConnection(CONFIG.database);

        connection.connect(function(err){
            if(!err) {
                var count = 0;
                var result = {};
                
                var query = 'SELECT * FROM '+CONFIG.table+' LIMIT '+start+','+size;
                connection.query(query, function(err, rows, fields) {
                    if(!err) {
						count++;
						result.data = rows;
						if(count == 2){
							setResultOnSuccess(result);
                        }
                    } else {
						count++;
						result.data = [];
						if(count == 2){
							setResultOnSuccess(result);
						}
                    }
                });
	
				connection.query('SELECT COUNT(*) AS count FROM '+CONFIG.database + '.' + CONFIG.table+';', function(err, rows, fields) {
					if(!err) {
						count++;
						result.count = rows[0].count;
						if(count == 2){
							setResultOnSuccess(result);
						}
					} else {
						count++;
						result.count = -1;
						if(count == 2){
							setResultOnSuccess(result);
						}
					}
				});
            
            } else {
                console.log("Error connecting database ... nn", err);

                var sender = new ResponseHandler(response, err);
                sender.send();

            }
        });
        
        function setResultOnSuccess(result) {
            'use strict';
			connection.end();
			var sender = new ResponseHandler(response, result);
			sender.send();
        }
        
    });

    app.get('/stock/company/:code', function (request, response) {
        var inputData = request.body;
        var code = request.params.code;

        var _mysql = new MySql();
        var connection =  _mysql.getConnection(CONFIG.database);

        //console.log(inputData);


        connection.connect(function(err){
            if(!err) {
                var query = 'SELECT * FROM '+CONFIG.table+' WHERE symbol ="'+code+'"';

                console.log(query)
                connection.query(query, function(err, rows, fields) {
                    connection.end();
                    if(!err) {
                        var sender = new ResponseHandler(response, rows);
                        sender.send()

                    } else {
                        // return error

                        var sender = new ResponseHandler(response, err);
                        sender.send()
                    }
                });


            } else {
                console.log("Error connecting database ... nn", err);

                var sender = new ResponseHandler(response, err);
                sender.send()

            }
        });


    });

    app.get('/realtime/stock/company', function (request, response) {
        var code = request.param('code');
        var cfunction = request.param('cfunction') || 'TIME_SERIES_MONTHLY';

        //https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=MSFT&apikey=demo
        var API_KEY = '5DTZIMY691P3MU65';

        var url = 'https://www.alphavantage.co/query?function=' + cfunction + '&symbol=' + code + '&apikey='+API_KEY;
        console.log(url);
        https.get(url, function (resp) {
            var data = '';
            resp.on('data', function(chunk){
                data += chunk;
            });

            resp.on('end', function () {
                var sender = new ResponseHandler(response, JSON.parse(data));
                sender.send()
            })
        })
            .on('error',function(err){
                var sender = new ResponseHandler(response, []);
                sender.send();

                console.log('error', err);
            })


    });

    // add a record
    app.post('/record', function (request, response) {
        console.log(request.body, request.params);
        var inputData = request.body;
        inputData.method = request.method;
        inputData.index = request.params;
        //console.log(inputData);
        var sender = new ResponseHandler(response, inputData);
        sender.send()
    });

    // application -------------------------------------------------------------
    // app.get('*', function (req, res) {
    //     res.sendFile(__dirname + '/public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    // });



    function getUrlVar(url, requestedKey) {
        "use strict";
        var vars = [], hashes, hash,
            temp_hashes,
            i;
        temp_hashes = url.replace("#", "");
        hashes = temp_hashes.split('&');
        for (i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        if (typeof requestedKey === 'undefined') {
            return vars;
        } else {
            return vars[requestedKey];
        }
    }
};

