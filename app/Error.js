var ResponseSender = require('./ResponseHandler');

var SERVER = SERVER || {};
SERVER.Error = function(response, data){
    this._response = response;
    this._data = data;
}
SERVER.Error.prototype.print = function(){
    var sender = new ResponseSender(this._response, this._data);
    sender.send();
}

module.exports = SERVER.Error;